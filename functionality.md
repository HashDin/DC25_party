## Functional Description


### Protocol Implementation level


- [ ] produce valid block headers
- [ ] simple recovery blocks, pubkey1 replaced by pubkey2 broadcasts pubkey3
- [ ] stores data in a content addressed way
- [ ] arbitrary key value attestations 
- [ ] 

### Storage
Due to time constraints and to show how awesome the protocol is we'll be using monogodb for our datastore.
Proving that even with full control of a database an attacker can not invalidate or change it's contents.
However this doesn't prevent censorship but we don't have time to do that properly.



### Peer Discovery

- [ ] None, Everyone will rely on a mongodb instance to be able to search for other users. This will not allow them to do anything but see their #D block headers. They can also ask for validation information

### User capabilities
Some of these overlap technically, some of the overlap is to highlight what should be simple to do in the UI


- [ ] sign arbitrary data onto their chain, limited by local storage and UI, hackers can hack though and do what they want
- [ ] trade access token
- [ ] set handle for identity
- [ ] broadcast their desire to be in the party
- [ ] see bids for entry 
- [ ] place bid for entry
- [ ] attest to taking door money
- [ ] issue tokens