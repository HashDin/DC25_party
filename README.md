# Cryptocoin Defcon 25 Party 
Crowdfunding a suite with the goal of hosting an exclusive party for cryptocurrency nerds. 
The token app will be a centralized MVP of [#D](https://hashd.in/hashd-in-draft0/), a decentralized identity protocol. 
There will be a tokens issued which represent access rights to the party.

### App Structure
This is supposed to be a demo of the #D protocol which is very distributed in nature but since we don't have time to do it right and this is only a proof of concept app there will be some centralized aspects.

There will be two main chunks of code to write the web client and the server/well known identities/reliable peers.

#### Server
The server will keep track of client ids and help peers find each other. This will be done using [libp2p webrtc star](https://github.com/libp2p/js-libp2p-webrtc-star) we will probably be able to use ipfs's public star server but it doesn't look too hard to run.
The server will be running a js-ipfs daemon and serving the Web client code via an IPFS gateway.
This will allow people to verify that the code they downloaded is the code we programmed in the wallets.
Other than keeping track of peers, serving static files, and running an IPFS daemon, the server doesn't do much, there maybe some state syncronization needed on top of IPFS which is needed to get peers who were offline up to speed with changes.


#### Web Clients 
The web clients will run their own IPFS node which will communicate to other peers directly over webRTC.
To ensure reliability they should have the affinity to the server's IPFS daemon as it will be a well known host to all peers.
The web clients will generate their own key pairs and mnemonic for back up key.
All of the crypto will be done client side. 
All the data will be stored in the user's browser's localStorage so it will be mostly limited to text and other small bits of data since it is capped at 5MB by browsers.




### Incentive Structure
All access rights will be exclusive to the investors who actually put money to toward the initial room reservation. 
This access gives them the right to the bedrooms at all times. 
This exlusive right is granted and its equivalent value will come out of the "crowdDebt" so ```Total rooms cost - equivalent bed cose = crowdDebt```

crowdDebt is the amount it costs over and above a normal room the goal is to extract enough value to bring that to zero or close to it.

#### Initial Token Distribution
Tokens will be distributed at random via physical nfc enabled wallets. 
The wallets are paper(tyvek), mostly orange with bitcoin and bitEvangel.org branding which work well for cash and credit cards. 
Each wallet has an embedded ntag213 nfc chips, each one will be programmed with a url and a secret key.
The secret key is just a large random string. 
When passed to a url it will load a page that has them do some taps and otherthings to add entropy to their private key.
The private key is then stored in some way which would allow for persistant identity between session stored on their web client. 
After generating their private key pair the site would then sign that secret key into their newly created #D identity chain and publish it to the network. 

#### Tokenization of secret keys
After invitees create their hashd identity by creating a key pair through the web app and if they are the first one to try to use that secret key they will automatically attest to having found that invite. 
When this attestation is published the Cryptocoin Party #D identity(CCP) will issue a token. 
This token will just be an attestation by CCP's blockchain stating that X's #D identity now has one token. 
This will allow them to then freely trade that token.

##### Preventing Double Spends
To trade tokens an identity would publish an attestation on their chain stating that Y's #D identity now has one token. 
In that attestation they include the block that they added CCP's attestation to their ID along with the information required to verify that. 
This inclusion of the sending attestation allows for quick verification. 
User's identities are unreliable, not online all the time, the CCP identity as well as project participant's ID's will be up 100% of the time and be well known to all identities which will be able to reliably listen to broadcasts, thus ensuring all transactions are seen. 
We could also broadcast double spend attempts and attest to that identity's shadiness. 
In addition to this we will also integrate the Open Timestamps Protcol by Peter Todd this will allow and identity vc to timestamp everyone's chain states very cheaply. 
This will peg #D chains to the Bitcoin's blockchain to further increase the immutability. 


#### Token value
Tokens will give you access, however there are many more tokens than there is room in the party for token holders. 
So as long as the party is not at capacity having a token will allow you entry. 
When it does reach capacity the highest bidder is first in line and the second highest bid will be second. 
Bids are not reset after someone leaves if there is still a line. 
Bids expire after 10 min so it can't stay too high for to long. 
Winning tokens will have 2 minutes to claim their slot in the party or it will go down the line until someone is present to be given entry.

When crowdDebt reaches 0 anyone who gave money to enter will get a partial refund in proportion to the amount they paid for each person paying to enter after the party breaks even. 
The last people will get the smallest refund and the last person to pay will get none. 
This can be adjusted so instead of all of the entry going to pay the room down immediately make it so that early comers get some of later party goers entry fees. 
The goal is to have an awesome party that is self sustaining, not to make money.


### Not Going to Jail
* The Tokens will fail every part of the [Howey Test](https://www.coinbase.com/legal/securities-law-framework.pdf)
* The suite will be paid for and reserved before any sale This makes it a non speculative investmnet
* Tokens will be initially distributed at random by encoding secret numbers on to bitwallets' nfc tags, which will be given out or placed randomly about Defcon. This makes it so that there is no initial monetary investment.
* Since this only grants access to a scarce resource there is no real common enterprise from which profits could be derived
 
