### Invitee Story: A Song of Crypto and Coins

Alice is an attendee to Defcon wandering about the con she finds a brightly colored Bitcoin branded tyvek wallet. 
Out of curiousity she grabs it, opens it. 
Inside she finds a business card designed to look like a ticket with instructions for redemption on the back. 
Instructions which tell her to scan the NFC tag embedded in the wallet. 


Scanning the wallet takes them to https://bitEvangel.org/CCP?sk=SECRET_KEY.


This page will give a longer decription of what they need to do to get into the party. 
Probably something to the gist of:
> Congrats, you figured out how to read an NFC tag. 
> Assuming you are the first one to scan this tag you will recieve a cryptographic token granting you access to the CryptoCoin Party(CCP). 
> If you are not the first you can always trade someone for a token.
> Clicking on the link below will take you to the alpha web client for the hashD ID protocol, you will be asked to add some entropy to generate your public key pair. 
> After that step is complete if you have a secret key it will be broadcast on the hashD network and you'll recieve a token good for entry into the party signed over to your ID.
> If you don't have a key you'll now have a hashD ID which allow you to recieve tokens if you trade for them.


Alice clicks the link under the description which takes her to a ccp.hashd.in?sk=SECRET_KEY subdomain. 
This page serves up the alpha javascript client.
Since this is the first time Alice has been to the site she sees a text field asking Alice to put in entropy.
After entering some random text and clicking generate keys, the keys are genreated locally. 
The first is saved to the localStorage of the device. 
The second is turned into a 12 word mnemonic and displayed with a message saying that her identity is saved on this device and if it is comprmised, lost or stolen she can login with the 12 words displayed.
Alice then clicks the continue button. 
The screen then loads another prompt with two choices which say ask her if she'd like to confirm her 12 word seed or live on the edge and continue. 
Either she confirms or continues, confirm she re-enters the seed then continues to the main app screen.


Since Alice created her identity with a secret that hasn't been seen before
the page redirects to which shows a minimal user interface that allows Alice to interact with the HashD Network.	

The interface shows a tab with labeled blocks and pending.

The blocks tab will show a list of block which can be expanded to show what data they contain. 
The first block shows that she attested to her public key and a hash of her backup public key.

Since she was the first one the web app has seen with that secret key it also automatically creates a new block and broadcasts.
This block contains the secret key. 

The pending tab contains a list of messages sent to the identity which she has option to accept and add them to her chain.
Alice sees a Crypto Coin Party Access Token issued by CCP.
She clicks accept which clears her pending tab and adds a new block to her #D chain. 


