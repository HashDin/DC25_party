## Tokens on #D

This token should be transferable, issuable by an identity, and verifible in a decentralize manner. 
There are many desirable properties to tokens and depending on the type and scenerio. 
Some of these properties are exclusive to each other.
Due to the extreme time contrainst we are only going to deliniate what is required to throw the party. 


Since #D identities are all equal in the eyes of the protocol any identity could reuse this format to issue their own token. 
The properties we are looking for in our token are, 
accountability, users can know that the token was issued by the identity and have proof of why it was issued.
Transferibility, users can freely transfer their token to another user in a manner that doesn't depend on the token issuer.
????


# #D Token Protocol Messages

## Reserved Keys
	
##### vd (validate)
Validate key is one which stores all of the information to validate the integrity of a tx in a merkle datastructure

	vd:{
		blockHash:"",
		txData:{},
		validatingHashes:[]
	}

##### ad (authenticate)
Authenitcate key is one which stores a set of data which allows you to confirm the authenticity of information


	ad:{
		blockHash:"",
		dataRoot:"",
		signature:""
	}

##### dd (auth and validate)
Auth and Validate contains the information form ad and vd
	
	dd:{
		blockHash:"",
		dataRoot:"",
		signature:"",
		txData:{},
		validatingHashes:[]
	}

**validatingHashes** - validatingHashes is an array that contains all of the hashes required to verify a tx within a block.
The list is in order from left to right in the merkle tree starting from the root.  



##### gd (grant data) NOT SOLID
Grant data is a transaction which allows you declare permissions on a specific data type
	
	gd:{
		target: a Blockhash of the identity which is recieving the permission or data type,
		verb: Arbitrary language i.e. "write",
		dataType: this would be a namespaced message type i.e. CCP.ccp.inviteIssueKey
	} 

Initially when an identity declares a new protocol or protocol instance anyone can write any message in the sub-protocol. 
Access control to message permissions can be enabled by the instance creator by broadcasting a gd(grant data) message, declaring an identity specific permission for a specific message type.
The first time a gd message is broadcast concerning a message type that message type deny by default, only allowing those declared in gd messages to write. 
This mechanism of access control could cause a fork where there is a permissioned instance and an open instance. 
When instance creators are being bad they can be punished by other nodes by creating a fork using a fd (fork data) tx which will declare what block they are forking another instance at. 
Voting for which fork would be as easy as broadcasting a fd tx whose forkPoint is the same as others people

##### fd (fork data)  NOT SOLID

	fd:{
		forkPoint: The block hash where you want to fork the instance of the specified datatype, all gd commands after that point will only be valid if published by the identity which publishes the fd block, i.e. ccpid31.ccp, would be the 31st block on ccpid's #D chain
		targetBranch: This is a block hash of the oldest block that they know of which has a forkPoint pointing to the same block hash.
	}

**targetBranch** - Each fd tx will reference the earliest block they know about which contains fd tx that points to the same forkPoint.
This will lead to eventual consistency where nodes reach consensus on what to call it based on whose block hash is earliest in the global, and who has gd command privilages. 
This eventual consensus isn't guaranteed as there will just be multiple branches at a fork.
Very much like git only changes make a difference so until multiple forks broadcast differing gd messages all of the forks of different names will be have the same validition rule set. 
This would allow for people 







#### Specific Protocols

Will be distinguished with a uri identity.datatype.objects
This gives us the ability control who and what can be added as valid to to this datastructure. 




#### ccpId.ccp Protocol(CryptoCoinParty)
	
	{
		ccpId:{
			ccp:{
			}
		}
	}



This will be the prefix for the rest of the messages that the Crypto Coin Party token protocol.
* **ccpId** would be one of the blocks on the ccp's id, this acts as a namespace for the format which allows people experiment without worrying about causing conflicts with existing datatypes defined by other identities. 
* **ccp** this would be the protocol/app identitier. 


All CCP.ccp token protocol

#### Invite issuance tx

This message allows a user to issue another secret key which will be valid for recieving an access token for the party. 
This must be issued before tokens maybe issued.	
	
	inviteIssueKey:"kn34i535298htrewike"

#### Found Key tx

	foundKey:"secretKeyString"


#### Token issuance tx

	tokenIssue:{
		CrypotCoinPartyAccess: random 256 bit tokenId,
		foundKeyBlock:{
			dd:{
				blockHash: The block hash that the invitee published the secret with,
				dataRoot:"",
				signature:"from invitee of dataRoot",
				txData:{ccpid.ccp.foundKey."secretKeyString"},
				validatingHashes:[]
		},
		inviteIssueKey:{
			dd:{
				blockHash: The block hash that issued the secret key,
				dataRoot:"",
				signature:"from invite issuer of dataRoot",
				txData:{ccpid.ccp.inviteIssueKey:"lksfdlkjlkjsalkj"},
				validatinls
				gHashes:[]	
				}
			}
		} 
	}
}

#### Token Transfer tx
	
	tokenTransfer:{
 		CrypotCoinPartyAccess: random 256 bit tokenId,
		txTo: blockHash from one of the blocks of the reciever,
		rxFrom: blockHash of the block that the current sender recieved theirs
	}







