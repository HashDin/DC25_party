Crypto Explained

### Creating an Idenitity 
Creating a #D identity only requires creating two private key pairs and creating a block.
One which is the live one is stored on the device via the localStorage api found in all modern browsers. 
The second is turned into to a 12 word mnemonic which the user will be instructed to write down. 
This second key is the revocation key for the first key. 
Once the keys are created the genesis block for that identity is created by creating one transaction which contains a key of "nextKey" and a value which is a hash of the second public key. 

Once the genesis block created it is then broadcast to the network. 

##### Block Structure
![Block Format](/images/blockFormat.jpg)

### Broadcast 
A broadcast is just a sending it to identities close to itself in the DHT. They can also broadcast to idenitites which have more proof of work since they are likely to be in more nodes' peer list. The DHT is constructed by the identity's block hash. Sending to any of the block hashes in an identity's chain will make it deliverable.

### Naming & Routing (WIP)
By making all links in #D chains routable it allows people to put a lot to put a lot of proof of work into specific block to be more easily found via XOR routing. In Kademalia's parlance the lowest hashes value(highest PoW), will have the smallest K-buckets. The values stored with the PoW block hashes is equal to the desired human readable name. If there are multiple id's claiming the same value then the one with less proof of work will have the leading numbers of the blockhash which are not zeros appended to it after 

	0000ab23421525 = CCP = CCP.4.ab
	0000ab10087869 = CCP = CCP.4.ab1
	000000ee596213 = CCP = CCP.6.
	00000386792921 = CCP = CCP.5.



 Addresses can be shared around via a gossip protocol as well.
