var fs = require('fs');
var express = require('express');

var port = 80;
var stylesheet = fs.readFileSync('public/css.css');
var app = express();

app.set('view engine', 'html');
app.engine('html', require('hbs').__express);

app.get('/', function(req, res) {
	fs.readFile('public/index.html', function(err, body) {
		res.end(body);
	});
});

app.get('/css.css', function(req, res) {
	res.setHeader('Content-Type', 'text/css');
	res.end(stylesheet);
});

app.get('/app.js', function(req, res) {
	fs.readFile('public/js/app.js', function(err, body) {
		res.end(body);
	});
});

app.get('/modal', function(req, res) {
	fs.readFile('views/modal.html', function(err, body) {
		res.end(body);
	});
});

app.get('/capabilities', function(req, res) {
	fs.readFile('views/capabilities.html', function(err, body) {
		res.end(body);
	});
});

app.get('/blocks', function(req, res) {
	fs.readFile('views/blocks.html', function(err, body) {
		res.end(body);
	});
});

app.get('/pending', function(req, res) {
	fs.readFile('views/pending.html', function(err, body) {
		res.end(body);
	});
});

app.get('/main', function(req, res) {
	fs.readFile('views/main.html', function(err, body) {
		res.end(body);
	});
});

app.get('/confirm', function(req, res) {
	fs.readFile('views/confirm.html', function(err, body) {
		res.end(body);
	});
});

app.listen(port, function() {
	console.log('Server running on port ' + port);
});
