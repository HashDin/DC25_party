
var keypair = require('keypair');
var rs = require('jsrsasign');

function generateKeypair() {
	var pair = keypair();
	if(typeof(Storage) !== "undefined") {
		localStorage.setItem("pk", pair.private);
	}
	else {
		console.log("Local storage not supported.");
	}
}

function getKeypair() {
	if(typeof(Storage) !== "undefined") {
		var priv = localStorage.getItem("pk");
		if(priv == null) {
			throw "Null pk";
		}
		return priv;
	}
	else {
		console.log("Local storage not supported.");
	}
}

function signData(privKey, data) {
	var sig = new rs.KJUR.crypto.Signature({"alg":"SHA256withRSA"});
	sig.init(privKey);
	sig.updateString(data);
	sigHex = sig.sign();
	return sigHex;
}

function verifySig(pubKey, sigHex, data) {
	var sig = new rs.KJUR.crypto.Signature({"alg":"SHA256withRSA"});
	sig.init(pubKey);
	sig.updateString(data);
	var isValid = sig.verify(sigHex);
	return isValid;
}

function load() {
	$('body').load('main');
	try {
		var privKey = getKeypair();
		console.log("Private Key from LocalStorage: " + privKey);
		$('#pkDiv').html(privKey);
	}
	catch(err) {
		$('#modal').modal('show');
	}
}

//How's it going?
//Enjoy the party!
//Also don't fuck with this, it's a party jackass

load();
